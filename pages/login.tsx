import styled from 'styled-components';
import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import React, { useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';

import { useUser } from '../contexts/User';
import AuthForm from '../components/AuthForm';
import Layout from '../components/Layout';

const LOGIN = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      email
    }
  }
`;

function Login(): JSX.Element {
  const router = useRouter();
  const { setUser } = useUser();
  const [login, { data, error, loading, called }] = useMutation(LOGIN, {
    onError: e => {
      if (e.graphQLErrors[0]?.extensions?.code === 'FORBIDDEN') {
        toast.error('Invalid email or password');
      }
    },
  });

  useEffect((): void => {
    if (called && !loading && !error) {
      toast.success('Successfully logged in');
      setUser({ email: data.login.email });
      router.push('/');
    }
  }, [called, loading]);

  return (
    <Layout>
      <Container>
        <h1>Login</h1>

        <FormContainer>
          <AuthForm
            onSubmit={(email, password): void => {
              login({ variables: { email, password } });
            }}
          />
        </FormContainer>

        <p>
          No account yet? { ' ' }
          <Link href="/register">
            <a>Create one now</a>
          </Link>
        </p>
      </Container>
    </Layout>
  );
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const FormContainer = styled.div`
  width: 150px;
  height: 150px;
  background-color: lightgrey;
  padding: 2em;
  border-radius: 8px;
`;

export default Login;
