import styled from 'styled-components';

import Layout from '../components/Layout';

const Index = (): JSX.Element => (
  <Layout>
    <Container>
      Page content
    </Container>
  </Layout>
);

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export default Index;
