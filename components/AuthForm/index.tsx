import { useState } from 'react';
import styled from 'styled-components';

interface Props {
  onSubmit: (email: string, password: string) => void;
}

function AuthForm({ onSubmit }: Props): JSX.Element {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <Form onSubmit={(e): void => {
      e.preventDefault();
      onSubmit(email, password);
    }}
    >
      <Input
        type="email"
        value={email}
        onChange={(e): void => setEmail(e.target.value)}
        placeholder="email"
      />

      <Input
        type="password"
        value={password}
        onChange={(e): void => setPassword(e.target.value)}
        placeholder="password"
      />

      <Button type="submit">
        Submit
      </Button>
    </Form>
  );
}

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  height: 100%;
`;

const Input = styled.input`
  padding: 1em;
  border: none;
  border-radius: 3px;
  outline: 0;
`;

const Button = styled.button`
  cursor: pointer;
`;

export default AuthForm;
