import { ApolloProvider } from '@apollo/react-hooks';
import { ApolloClient, InMemoryCache, HttpLink, NormalizedCacheObject, ApolloLink } from 'apollo-boost';
import { onError } from 'apollo-link-error';
import fetch from 'isomorphic-unfetch';
import { NextPageContext } from 'next';
import Head from 'next/head';
import React from 'react';
import { toast } from 'react-toastify';

const GRAPHQL_URI: string = process.env.API_BASE_URL || '';
let apolloClient: ApolloClient<NormalizedCacheObject> | null = null;

const createApolloClient = (initialState = {}): ApolloClient<NormalizedCacheObject> => {
  const isBrowser = typeof window !== 'undefined';

  const errorLink = onError(({ networkError }) => {
    if (isBrowser && networkError) {
      toast.error('Network error');
    }
  });

  const httpLink = new HttpLink({
    uri: GRAPHQL_URI,
    credentials: 'include',
    fetch: isBrowser ? undefined : fetch,
  });

  return new ApolloClient({
    connectToDevTools: isBrowser,
    ssrMode: !isBrowser,
    link: ApolloLink.from([errorLink, httpLink]),
    cache: new InMemoryCache().restore(initialState),
  });
};

const initApolloClient = (initialState = {}): ApolloClient<NormalizedCacheObject> => {
  const isBrowser = typeof window !== 'undefined';

  if (isBrowser) {
    return createApolloClient(initialState);
  }

  if (!apolloClient) {
    apolloClient = createApolloClient(initialState);
  }

  return apolloClient;
};

interface Props
{
  apolloClient: ApolloClient<NormalizedCacheObject>;
  apolloState: NormalizedCacheObject;
  pageProps: React.ReactNode;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const withApollo = (PageComponent: any,
  { ssr = true } = {}): ({ apolloClient, apolloState, ...pageProps }: Props) => JSX.Element => {
  // eslint-disable-next-line no-shadow,react/prop-types
  const WithApollo = ({ apolloClient, apolloState, ...pageProps }: Props): JSX.Element => {
    const client = React.useMemo(
      (): ApolloClient<NormalizedCacheObject> => apolloClient || initApolloClient(apolloState),
      [],
    );
    return (
      <ApolloProvider client={client}>
        <PageComponent {...pageProps} />
      </ApolloProvider>
    );
  };

  if (typeof window === 'undefined') {
    if (ssr) {
      WithApollo.getInitialProps = async (ctx: NextPageContext):
      Promise<{apolloState: NormalizedCacheObject}> => {
        const { AppTree } = ctx;

        let pageProps = {};
        if (PageComponent.getInitialProps) {
          pageProps = await PageComponent.getInitialProps(ctx);
        }

        // eslint-disable-next-line no-shadow
        const apolloClient = initApolloClient(undefined);

        try {
          // eslint-disable-next-line global-require
          await require('@apollo/react-ssr').getDataFromTree(
            <AppTree
              pageProps={{ apolloClient }}
              {...pageProps}
              {...PageComponent}
            />,
          );
        } catch (error) {
          // eslint-disable-next-line no-console
          console.error('Error while running `getDataFromTree`', error);
        }

        Head.rewind();

        const apolloState = apolloClient.cache.extract();

        return {
          ...pageProps,
          apolloState,
        };
      };
    }
  }

  return WithApollo;
};

export default withApollo;
